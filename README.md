# Identity Chain Tools
This repository contains some simple Bash scripts for generating Authority and
Signing Keys and setting up an Identity Chain for use with the [Factomize
XenForo Add
on](https://bitbucket.org/canonical-ledgers/factomize-xenforo-add-on/src/master/).

## Requirements
The following programs must be in your path.
- `bash`
- `factom-cli`
- `openssl`

You must also have access to the `factomd` and `factom-walletd` JSON RPC APIs.
These scripts assume that these endpoints are at the usual locations:
`localhost:8088` and `localhost:8089` respectively. You must have a funded
Entry Credit address stored with `factom-walletd`.

## Set up a new Identity Chain
Run `./setup-identity-chain <EC ADDRESS>` and follow the instructions to
generate a new Authority Key.

This will output two `curl` commands that you must copy and run on a computer
with access to `factomd`. The response from the second `curl` command will
include a Chain ID. Copy this chain id for use in the next step.

Wait for this chain to make it into a block.

## Set up a new Signing Key
Next you need to tell the Factomize add-on to generate a new Signing Key. Go to
the options page for the Factomize add-on by going to the Xenforo Admin Control
Panel, then clicking on Add-ons. Click the "gear" icon to the right of the
Factomize add-on and then click "Options".

At the bottom of the Factomize options page, click the checkbox "Generate New
Signing Key" and then click "Save" at the bottom of the page. When the page
reloads, a new public key will appear in the text box at the bottom of the
page. Use the "Copy to Clipboard" button to copy this public key. Create a new
file on your system with the public key you just copied.

Now run `./setup-signing-key <AUTHORITY PRIVATE KEY> <SIGNING PUBLIC KEY>
<CHAIN ID> <EC ADDRESS> ` to sign the public Signing Key with your Authority
Key and output the `curl` commands to commit and reveal the entry on your
Identity Chain.

This command will again output two `curl` commands that must be run on a
computer with access to `factomd`.

If the Factomize add-on has been properly configured, it will start using the
new Signing Key when it sees this entry on the Factom blockchain.
