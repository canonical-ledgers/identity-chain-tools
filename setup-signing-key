#!/bin/bash
set -e


sign_priv_key="signing_private_key.pem"
sign_pub_key="signing_public_key.pem"

umask 177
tmp_priv_key=$(mktemp)
tmp_pub_key_sig=$(mktemp)
rm_tmp() {
    rm -f "$tmp_priv_key" "$tmp_pub_key_sig"
}
trap rm_tmp EXIT

generate_signing_keys() {
    echo "Generating a PRIVATE SIGNING KEY for signing entries."
    # Generate authority key
    openssl genrsa -out "$tmp_priv_key" 4096
    echo "Enter a password to encrypt your PRIVATE SIGNING KEY..."
    # Encrypt private key
    openssl rsa -des3 -in "$tmp_priv_key" -out "$sign_priv_key"
    # Output public key
    openssl rsa -in "$tmp_priv_key" -pubout -out "$sign_pub_key"
    echo
    echo "Success: Your encrypted PRIVATE SIGNING KEY is located at: '$sign_priv_key'"
    echo "Success: Your PUBLIC SIGNING KEY is located at: '$sign_pub_key'"
    echo
}

compose_signing_key_entry() {
    local authority_priv_key="$1"
    local signing_pub_key="$2"
    local CHAINID="$3"
    # Sign public key
    openssl dgst -sha512 \
        -sign "$authority_priv_key" \
        -out "$tmp_pub_key_sig" \
        "$signing_pub_key"

    # External IDs:
    local ExtID1="RSA-SHA512"
    local ExtID2="$(xxd -p -c 1024 $tmp_pub_key_sig)"
    local ExtID3="Entry Signing Key"
    # Content: authority public key
    local content="$(cat $signing_pub_key)"

    echo "$content" | \
        factom-cli composeentry \
            -c "$CHAINID" \
            -e "$ExtID1" \
            -x "$ExtID2" \
            -e "$ExtID3" \
            $ECADDRESS
}


usage() {
    echo "Usage: $0 <AUTHORITY PRIVATE KEY> <SIGNING PUBLIC KEY> <CHAIN ID> <EC ADDRESS>"
    echo "    Establishes <SIGNING PUBLIC KEY> as a new Signing Key "
    echo "    on the Identity Chain <CHAIN ID> signed by the "
    echo "    <AUTHORITY PRIVATE KEY>."
}
if [[ -z "$1" || -z "$2" || -z "$3" || -z "$4" ]]; then
    usage
    exit 1
fi
auth_priv_key=$1; shift
sign_pub_key=$1; shift
chainid=$1; shift
ECADDRESS=$1; shift
#generate_signing_keys
compose_signing_key_entry "$auth_priv_key" "$sign_pub_key" "$chainid"
